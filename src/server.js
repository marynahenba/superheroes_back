const mongoose = require('mongoose');
const app = require('./app');
const PORT = 8080;

/* Connecting to the database and then starting the server. */
mongoose
	.connect(
		'mongodb+srv://marynas:sMaM8orQbiCj18ol@marynascluster.lwungkp.mongodb.net/superheroes?retryWrites=true&w=majority'
	)
	.then(() => {
		app.listen(PORT, console.log(`Server started on port ${PORT}`));
	})
	.catch((err) => {
		console.log(err);
	});
