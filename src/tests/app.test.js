const mongoose = require('mongoose');
const request = require('supertest');
const app = require('../app');

beforeEach(async () => {
	await mongoose.connect(
		'mongodb+srv://marynas:sMaM8orQbiCj18ol@marynascluster.lwungkp.mongodb.net/superheroes?retryWrites=true&w=majority'
	);
});

afterEach(async () => {
	await mongoose.connection.close();
});

describe('GET /api/superheroes', () => {
	it('should return all heroes', async () => {
		const res = await request(app).get('/api/superheroes');
		expect(res.statusCode).toBe(200);
		expect(res.body.heroes).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					nickname: expect.any(String),
					real_name: expect.any(String),
					origin_description: expect.any(String),
					superpowers: expect.any(String),
					catch_phrase: expect.any(String),
					img: expect.any(Object),
				}),
			])
		);
	});
});

describe('GET /api/superheroes/:id', () => {
	it('should return a Batman hero', async () => {
		const res = await request(app).get(
			'/api/superheroes/635523ce8c22721596de59ed'
		);
		expect(res.statusCode).toBe(200);
		expect(res.body.nickname).toBe('Superman');
	});
});

describe('PATCH /api/superheroes/:id', () => {
	it('should update a hero', async () => {
		const res = await request(app)
			.patch('/api/superheroes/635523ce8c22721596de59ed')
			.send({
				nickname: 'Superman Updated',
				real_name: 'Clark Kent',
				origin_description:
					"he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction...",
				superpowers:
					'solar energy absorption and healing factor, solar flare and heat vision, solar invulnerability, flight...',
				catch_phrase:
					"“Look, up in the sky, it's a bird, it's a plane, it's Superman!”",
			});
		expect(res.statusCode).toBe(200);
		expect(res.body.nickname).toBe('Superman Updated');
		expect(res.body.real_name).toBe('Clark Kent');
	});
});

describe('PATCH /api/superheroes/:id', () => {
	it('should update a hero', async () => {
		const res = await request(app)
			.patch('/api/superheroes/635523ce8c22721596de59ed')
			.send({
				nickname: 'Superman',
				real_name: 'Clark Kent',
				origin_description:
					"he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction...",
				superpowers:
					'solar energy absorption and healing factor, solar flare and heat vision, solar invulnerability, flight...',
				catch_phrase:
					"“Look, up in the sky, it's a bird, it's a plane, it's Superman!”",
			});
		expect(res.statusCode).toBe(200);
		expect(res.body.nickname).toBe('Superman');
		expect(res.body.real_name).toBe('Clark Kent');
	});
});

describe('DELETE /api/superheroes/:id', () => {
	it('should delete a hero', async () => {
		const res = await request(app).delete(
			'/api/superheroes/6355c6ca26daea4f75805fb9'
		);
		expect(res.statusCode).toBe(200);
	});
});

describe('POST /api/superheroes', () => {
	it('should create a hero', async () => {
		request.files = {
			fieldname: 'img',
			originalname: 'hulk.png',
			encoding: '7bit',
			mimetype: 'image/png',
			destination: './images',
			filename: 'hulk.png',
			path: 'images/hulk.png',
			size: 44335,
		};
		const res = await request(app).post('/api/superheroes').send({
			nickname: 'Superman',
			real_name: 'Clark Kent',
			origin_description:
				"he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction...",
			superpowers:
				'solar energy absorption and healing factor, solar flare and heat vision, solar invulnerability, flight...',
			catch_phrase:
				"“Look, up in the sky, it's a bird, it's a plane, it's Superman!”",
			img: request.files,
		});
		expect(res.statusCode).toBe(201);
		expect(res.body.nickname).toBe('Superman');
		expect(res.body.real_name).toBe('Clark Kent');
		expect(res.body.origin_description).toBe(
			"he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction..."
		);
		expect(res.body.catch_phrase).toBe(
			"“Look, up in the sky, it's a bird, it's a plane, it's Superman!”"
		);
	});
});
