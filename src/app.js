const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

const corsOptions = {
	origin: '*',
	credentials: true,
	optionSuccessStatus: 200,
};

const { superheroesRouter } = require('./controllers/superheroesRouter');

app.use(cors(corsOptions));
app.use(
	cors({
		methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
	})
);
app.use(express.json());
app.use(morgan('dev'));

/* This is the root route. It is used to check if the server is running. */
app.get('/', (req, res) => {
	res.status(200).json({ alive: 'True' });
});

/* Telling the server to use the routes in the ProductRoutes file. */
app.use('/api/superheroes', superheroesRouter);

module.exports = app;
