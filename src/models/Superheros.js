const mongoose = require('mongoose');
mongoose.set('debug', true);

const Superhero = mongoose.model('superheros', {
	nickname: {
		type: String,
		required: true,
	},
	real_name: {
		type: String,
		required: true,
	},
	origin_description: {
		type: String,
		required: true,
	},
	superpowers: {
		type: String,
		required: true,
	},
	catch_phrase: {
		type: String,
		required: true,
	},
	// images: {
	// 	type: Array,
	// 	img: { data: Buffer, contentType: String },
	// },
	img: {
		data: Buffer,
		contentType: String,
	},
});

module.exports = { Superhero };
