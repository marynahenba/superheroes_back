const express = require('express');
const router = express.Router();
const fs = require('fs');

const {
	getAllHeroes,
	createHero,
	getHeroById,
	updateHero,
	deleteHero,
} = require('../services/superheroesService');

const { upload } = require('../middleware/uploadMiddleware');
const { asyncMiddleware } = require('../middleware/asyncMiddleware');

router.get(
	'/',
	asyncMiddleware(async (req, res) => {
		const page = parseInt(req.query.page);
		const limit = parseInt(req.query.limit);

		const { results, message } = await getAllHeroes(limit, page);

		if (message) return res.status(400).send({ message });

		res.json(results);
	})
);

router.post(
	'/',
	// upload.array('images', 10),
	upload.single('img'),
	asyncMiddleware(async (req, res) => {
		const {
			nickname,
			real_name,
			origin_description,
			superpowers,
			catch_phrase,
		} = req.body;
		// const images = req.files;
		const img = req.file;

		await createHero(
			nickname,
			real_name,
			origin_description,
			superpowers,
			catch_phrase,
			img
		);

		if (
			!nickname ||
			!real_name ||
			!origin_description ||
			!superpowers ||
			!catch_phrase
		) {
			return res.status(400).send({ message: 'Content can not be empty!' });
		}

		res.json({ message: 'Hero created successfully' });
	})
);

router.get(
	'/:id',
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		const hero = await getHeroById(id);

		if (!hero)
			return res.status(400).send({ message: `Hero with ${id} is not found` });

		return res.json(hero);
	})
);

router.patch(
	'/:id',
	upload.single('img'),
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		if (req.file) {
			const payload = {
				...req.body,
				img: {
					data: fs.readFileSync('./images/' + req.file.filename),
					contentType: req.file.contentType,
				},
			};

			const updatedHero = await updateHero(id, payload);

			if (!updatedHero)
				return res
					.status(400)
					.send({ message: `Hero with ${id} is not found` });

			res.json({ message: 'Hero details is updated succsessfully' });
		} else {
			const payload = req.body;

			const updatedHero = await updateHero(id, payload);

			if (!updatedHero)
				return res
					.status(400)
					.send({ message: `Hero with ${id} is not found` });

			res.json({ message: 'Hero details is updated succsessfully' });
		}
	})
);

router.delete(
	'/:id',
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		const deletedHero = await deleteHero(id);

		if (!deletedHero)
			return res.status(400).send({ message: `Hero with ${id} is not found` });

		res.json({ message: 'Hero deleted succsessfully' });
	})
);

module.exports = {
	superheroesRouter: router,
};
