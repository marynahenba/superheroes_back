const fs = require('fs');
const { Superhero } = require('../models/Superheros');

const getAllHeroes = async (limit, page) => {
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const results = {};

	if (endIndex < (await Superhero.countDocuments().exec())) {
		results.next = {
			page: page + 1,
			limit: limit,
		};
	}

	if (startIndex > 0) {
		results.previous = {
			page: page - 1,
			limit: limit,
		};
	}

	results.heroes = await Superhero.find().limit(limit).skip(startIndex).exec();

	if (results.heroes.length === 0) return { message: 'Data base is empty' };

	return { results };
};

const createHero = async (
	nickname,
	real_name,
	origin_description,
	superpowers,
	catch_phrase,
	img
) => {
	const image = {
		data: fs.readFileSync('./images/' + img.filename),
		contentType: img.contentType,
	};

	const newHero = new Superhero({
		nickname,
		real_name,
		origin_description,
		superpowers,
		catch_phrase,
		img: image,
	});

	await newHero.save();
	return newHero;
};

const getHeroById = async (heroId) => {
	const hero = await Superhero.findById({ _id: heroId });

	return hero;
};

const updateHero = async (heroId, payload, img) => {
	const updatedHero = await Superhero.findByIdAndUpdate(
		{ _id: heroId },
		payload
	);
	await updatedHero.save();
	return updatedHero;
};

const deleteHero = async (heroId) => {
	const deletedHero = await Superhero.findByIdAndDelete({ _id: heroId });
	return deletedHero;
};

module.exports = {
	getAllHeroes,
	createHero,
	getHeroById,
	updateHero,
	deleteHero,
};
