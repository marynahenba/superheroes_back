const multer = require('multer');

const path = require('path');
const fs = require('fs');
const dir = './images';
const maxSize = 0.1 * 1024 * 1024;

const upload = multer({
	storage: multer.diskStorage({
		destination: (req, file, cb) => {
			if (!fs.existsSync(dir)) {
				fs.mkdirSync(dir);
			}
			cb(null, './images');
		},
		filename: (req, file, cb) => {
			cb(null, file.originalname);
		},
	}),

	fileFilter: (req, file, cb) => {
		let ext = path.extname(file.originalname);
		if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
			return cb(null, false);
		}
		cb(null, true);
	},
	limits: maxSize,
});

module.exports = { upload };
