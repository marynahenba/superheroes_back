const asyncMiddleware = (callback) => (req, res, next) => {
	callback(req, res).catch(next);
};

module.exports = {
	asyncMiddleware,
};
