# Getting Started with Create React App

The backend side of the Superheroes App is implemented with NodeJS and Express.js. To start using App run { npm start }, then go to the frontend repository (https://gitlab.com/marynahenba/superheroes_front), and follow the instructions in the README file.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

### `npm dev`

Runs the app in the development mode using Nodemon.

### `npm test`

Runs the tests of app, implemented with Jest and covered the main logic of CRUD operations.